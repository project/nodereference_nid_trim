
Description:
The Nodereference autocomplete nid trim  module is a simple module that uses JavaScript 
to remove the [nid:X] text when you select an item in a Node Reference field.

For example, if you have My Node Title [nid:384] in the input, jquery_autocomplete changes 
it to just My Node Title.

The module is very small and the development was started by logicalor 
which can be found here:
http://drupal.org/node/640832#comment-4082652

Requirements:
Drupal 6.x
CCK

Installation:
1. Copy the extracted nodereference_nid_trim directory to your Drupal sites/all/modules directory.
2. Login as an administrator. Enable the Node Reference module as well as this module at 
http://www.example.com/?q=admin/build/modules
3. Add "Node reference" field to the desired Content type. For example "Page" the field can be 
created at http://www.example.com/?q=admin/content/node-type/page/fields.

Support:
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/nodereference_nid_trim