var nodereference_ac_loaded = false;
var process_ac = true;
Drupal.behaviors.jqueryAutocomplete = function (context) {
	$('.nodereference-jquery-ac', context).each(function() {
		var ac = $(this);
		ac.data('changed', false);
		ac.data('alias', 'ac-real-');
		ac.data('rx',  /\s*\[nid:\d+\]\s*/i);
		ac.data('name', ac.attr('name'));
		ac.data('id', ac.attr('id'));
		ac.data('value', ac.val());
		ac.before('<input name="'+ac.data('alias')+ac.data('name')+'" type="hidden" id="'+ac.data('alias')+ac.data('id')+'" value="'+ac.data('value')+'" />');
		ac.data('real', '#'+ac.data('alias')+ac.data('id'));
	});
	$('.nodereference-jquery-ac', context).blur(function() {
		var ac = $(this);
		var real = $(ac.data('real'));
    if ($.browser.msie && !process_ac) {
      if (ac.val().match(ac.data('rx'))) {
        real.val(ac.val());
      }
      ac.val(ac.val().replace(ac.data('rx'), ''));
      ac.attr('name', ac.data('alias')+ac.data('name'));
      real.attr('name', ac.data('name'));
      return;
    }
		if ($.browser.msie && !ac.data('changed')) {
			ac.change();
		}
		if (ac.val().match(ac.data('rx'))) {
			real.val(ac.val());
		}
		ac.val(ac.val().replace(ac.data('rx'), ''));
		ac.attr('name', ac.data('alias')+ac.data('name'));
		real.attr('name', ac.data('name'));
    if ($.browser.msie && ac.data('changed')) {
      process_ac = false;
    }
		ac.data('changed', false);
	});
	$('.nodereference-jquery-ac', context).change(function() {
		var ac = $(this);
		if ($.browser.msie) {
			ac.data('changed', true);
		}
		ac.blur();
	});
  if (!nodereference_ac_loaded && ($.browser.safari || $.browser.mozilla)) {
    nodereference_ac_loaded = true;
    jsACOriginal = Drupal.jsAC.prototype.hidePopup;
    Drupal.jsAC.prototype.hidePopup = function (keycode) {
      jsACOriginal.call(this, keycode);
      var ac_id = this.input.id;
      var ac = $('#' + ac_id);
      var real = $(ac.data('real'));
      if (ac.val().match(ac.data('rx'))) {
        real.val(ac.val());
      }
      ac.val(ac.val().replace(ac.data('rx'), ''));
      ac.attr('name', ac.data('alias')+ac.data('name'));
      real.attr('name', ac.data('name'));
    };
  }
	$('.nodereference-jquery-ac', context).focus(function() {
		var ac = $(this);
		var real = $(ac.data('real'));
		ac.attr('name', ac.data('name'));
		real.attr('name', ac.data('alias')+ac.data('name'));
    if ($.browser.msie) {
      process_ac = true;
    }
	});
	$('.nodereference-jquery-ac', context).each(function() {
		var ac = $(this);
		ac.blur();
	});
};